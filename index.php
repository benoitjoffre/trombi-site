<?php
require_once('functions.php');

$formateurs = getFormateurs();
$apprenants = getApprenants();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/style.css">
    <title>INDEX</title>
</head>
<body>

<!--HEADER-->
<header>
    <div class="header">
            <img class="img-simplon" src="img/simplon-oc.png" alt="">
            <h1 class="title">Développeur Web et Mobile <br>Promotion II - CAHORS</h1>
            <img class="img-ern" src="img/ern-logo.png" alt="">
    </div>
</header>



<!--SEARCH-->
<div class="search-div">
        <form action="/search" method="get">
            <input class="search" name="search" type="text" placeholder="Rechercher" />
            <input id="search-btn" type="submit" value="OK" />
        </form>
</div>




<div class="trombi">

<!--FORMATEUR-->
    <h2>FORMATEUR.ICE</h2>
    <div class="formateur">
            <?php foreach($formateurs as $formateur): ?>



            <div class="card">
                <a href="result.php?id=<?= $formateur->id ?>"><div class="round"></div></a>
                <p><?= $formateur->fname ?></p>
                <p><?= $formateur->lname ?></p>
            </div>

            <?php endforeach ?>

    </div>


<!--APPRENANTS-->

<h2>APPRENANTS.ES</h2>
    <div class="student">

       <?php foreach($apprenants as $apprenant): ?>



<div class="card">
    <a href="result.php?id=<?= $apprenant->id ?>"><div class="round"></div></a>
    <p><?= $apprenant->fname ?></p>
    <p><?= $apprenant->lname ?></p>
</div>

<?php endforeach ?>
        
    </div>

</div>
</div>

</body>
</html>