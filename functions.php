<?php
// FONCTION QUI RECUPERE TOUTES LES INFOS DES FORMATEURS
function getFormateurs()
{
    require('config/db.php');
    $req = $bdd->prepare('SELECT id, fname, lname, pseudo FROM formateurs ORDER BY id DESC');
    $req->execute();
    $data = $req->fetchAll(PDO::FETCH_OBJ);
    return $data;
    $req->closeCursor();
}

// FONCTION QUI RECUPERE TOUTES LES INFOS DES APPRENANTS
function getApprenants()
{
    require('config/db.php');
    $req = $bdd->prepare('SELECT * FROM apprenants ORDER BY id DESC');
    $req->execute();
    $data = $req->fetchAll(PDO::FETCH_OBJ);
    return $data;
    $req->closeCursor();
}



// FONCTION QUI RECUPERE TOUS LES ID DES FORMATEURS
function getFormateur($id)
{
    require('config/db.php');
    $req = $bdd->prepare('SELECT * FROM formateurs WHERE id = ?');
    $req->execute(array($id));

    if($req->rowCount() == 1)
    {
        $data = $req->fetchAll(PDO::FETCH_OBJ);
        return $data; 
    }else
        header('location:index.php');
    
}

// FONCTION QUI RECUPERE TOUS LES ID DES APPRENANTS
function getApprenant($id)
{
    require('config/db.php');
    $req = $bdd->prepare('SELECT * FROM apprenants WHERE id = ?');
    $req->execute(array($id));

    if($req->rowCount() == 1)
    {
        $data = $req->fetchAll(PDO::FETCH_OBJ);
        return $data; 
    }
    
}